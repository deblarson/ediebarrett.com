<?php
global $table_prefix;

// Development mode
define('DEV', $_SERVER['SERVER_NAME'] == 'localhost');
if (DEV) {
  define('WP_DEBUG', true);
  define('WP_DEBUG_LOG', true);
  define('SCRIPT_DEBUG', true);
  ini_set('display_errors', true);
  ini_set('display_startup_errors', true);
  error_reporting(E_ALL);
}

// Allow big uploads.
ini_set('upload_max_filesize', '64M');
ini_set('post_max_size', '64M');

$wp_home = (DEV ? 'http://' : 'https://') . $_SERVER['SERVER_NAME'];
if (DEV) $wp_home .= ':' . $_SERVER['SERVER_PORT'];
define('WP_HOME', $wp_home);

// .htaccess rewrites to the WordPress root
define('WP_SITEURL', $wp_home);

// The content directory
define('WP_CONTENT_DIR', dirname(ABSPATH) . '/content');

// MySQL env vars
require 'auth.php';

// MySQL settings
$table_prefix = '3f9_';
define('DB_HOST', 'localhost');
define('DB_NAME', DEV ? 'mysql' : getenv('DB_NAME'));
define('DB_USER', DEV ? 'root' : getenv('DB_USER'));
define('DB_PASSWORD', DEV ? 'root' : getenv('DB_PASS'));
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');

// Authentication keys/salts for cookies
define('AUTH_KEY',         '9+gGx{i_z4:5;`*^uYc[T]E/aLE<L2|#R-gGVBr@uaj-6rs=N8cmTaMf-fpQ/8BG');
define('SECURE_AUTH_KEY',  ',M1TcxLdVTvp-S!s*}_f/mur8s.KIIFR}7oE|mb<H|QEN7Ick8dvcYLy-7v.i+!-');
define('LOGGED_IN_KEY',    '9|v6$;xGwtk{i]Ym8:nFR ~`k9,;sboWQI7XmFUNiPK6Gn70f.4oU]d2`EpyuanI');
define('NONCE_KEY',        'EPb|9j2V<=^5b<G^/>8Ai;3@D-m0fqYmo2jhc=@kPNtmV?ss(Wy:i#%o1Afpj3kr');
define('AUTH_SALT',        '?,RI}1F]jVdw`hrWS`O|JV2!3Kfk<>Ziz^)yU@g~r-B+a|M<LXLa G)2~W;&|o{h');
define('SECURE_AUTH_SALT', 'v,v=U[U-{~2>2,Dy+a#z<5B6K,+x:vNoeV-@!4gDF{JISHb+>0e( y~MO&4>i- v');
define('LOGGED_IN_SALT',   ')Sh%- MH}5>m8<7D>^A~&kk%r#q|*.;>E?O7if`HzeO:CWm-Ftb^|<|}{e@7l/Gs');
define('NONCE_SALT',       'S 4AB*w-s%_Tg;c~jr_|@5AKes 4IHl_)LSxAs1NbZ@e]O^^OB*MWsK9RTd)F4]}');


// Intialize wordpress core
require_once ABSPATH . 'wp-settings.php';

// Enable MySQL debugging
if (DEV) {
  $general_log_file = WP_CONTENT_DIR . '/sql.log';
  $wpdb->show_errors();
  $wpdb->query("
    SET GLOBAL
      general_log = 1,
      general_log_file = '$general_log_file',
      table_definition_cache = 400;
  ");
}
