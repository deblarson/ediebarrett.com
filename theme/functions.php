<?php

// Enable parent theme's must-use plugins.
add_mu_plugins(get_template_directory() . '/mu-plugins');

// Enable project's must-use plugins.
add_mu_plugins(WPMU_PLUGIN_DIR);

// Enable debugging plugins.
if (DEV && is_admin()) {
  $dev_plugins = WP_CONTENT_DIR . '/dev-plugins/*';
  foreach (glob($dev_plugins) as $plugin) {
    require_once get_plugin_info($plugin)['main'];
  }
}

// Enable the child theme
if (!is_admin()) {
  add_action('wp_enqueue_scripts', 'theme_enqueue_styles', 100);
  function theme_enqueue_styles() {
    $deps = array();
    if (is_child_theme()) {
      $parent_css = get_template_directory_uri() . '/style.css';
      wp_enqueue_style('parent-styles', $parent_css);
      array_push($deps, 'parent-styles');
    }

    $theme_css = get_stylesheet_directory_uri() . '/style.css';
    $theme_ver = wp_get_theme()->get('Version');
    wp_enqueue_style('theme-styles', $theme_css, $deps, $theme_ver);
  }
}

// Move all <script> tags to the end of <body>
add_action('wp_enqueue_scripts', 'move_scripts_to_body', INF);
function move_scripts_to_body() {
  remove_action('wp_head', 'wp_print_scripts');
  remove_action('wp_head', 'wp_print_head_scripts', 9);
  remove_action('wp_head', 'wp_enqueue_scripts', 1);

  add_action('wp_footer', 'wp_print_scripts', 5);
  add_action('wp_footer', 'wp_enqueue_scripts', 5);
  add_action('wp_footer', 'wp_print_head_scripts', 5);
}

function get_plugin_info($plugin_dir) {
  $plugin_dir = trailingslashit($plugin_dir) . '*.php';
  foreach (glob($plugin_dir) as $main) {
    $info = get_file_data($main, array(
      'name' => 'Plugin Name',
      'author' => 'Author',
      'author_uri' => 'Author URI',
      'description' => 'Description',
      'version' => 'Version',
    ));
    if ($info['name']) {
      $info['main'] = $main;
      return $info;
    }
  }
}

// Helper for auto-installation of must-use plugins.
function add_mu_plugins($plugins_dir) {
  if (!is_dir($plugins_dir)) return;
  $plugins = array();
  $plugins_dir = trailingslashit($plugins_dir) . '*';
  foreach (glob($plugins_dir) as $plugin) {
    if (!is_dir($plugin)) continue;
    $info = get_plugin_info($plugin);
    if ($info) {
      $name = basename($plugin);
      $plugins[$name] = $info;
    }
  }
  if (!empty($plugins)) {
    @mkdir(WPMU_PLUGIN_DIR);
    foreach ($plugins as $name => $info) {
      include_once $info['main'];
      $main = path_relative(WPMU_PLUGIN_DIR, $info['main']);
      @file_put_contents(
        WPMU_PLUGIN_DIR . "/$name.php",
        "<?php\n" .
        "/*\n" .
        "  Plugin Name: {$info['name']}\n" .
        "  Author: {$info['author']}\n" .
        "  Author URI: {$info['author_uri']}\n" .
        "  Description: {$info['description']}\n" .
        "  Version: {$info['version']}\n" .
        "*/\n\n" .
        "include_once dirname(__FILE__) . '/$main';\n"
      );
    }
  }
}

function path_relative($dir, $dest) {
  $ds = DIRECTORY_SEPARATOR;
  $dir = explode($ds, rtrim($dir, $ds));
  $dest = explode($ds, $dest);
  while ($dir && $dest && ($dir[0] == $dest[0])) {
    array_shift($dir);
    array_shift($dest);
  }
  return str_repeat('..'.$ds, count($dir)) . implode($ds, $dest);
}
